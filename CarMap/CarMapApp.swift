//
//  CarMapApp.swift
//  CarMap
//
//  Created by user on 19.03.2021.
//

import SwiftUI

@main
struct CarMapApp: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}
