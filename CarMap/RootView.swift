//
//  ContentView.swift
//  CarMap
//
//  Created by user on 19.03.2021.
//

import SwiftUI

struct RootView: View {
    @State var flagTo = false
    var body: some View {
        if !flagTo  {
            Text("Tap me!")
                .foregroundColor(.blue)
                .padding()
                .onTapGesture {
                    flagTo.toggle()
                }
        } else {
            MainView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
