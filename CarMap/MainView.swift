//
//  MainView.swift
//  CarMap
//
//  Created by user on 19.03.2021.
//

import SwiftUI
import MapKit
import CoreLocation

struct MainView: View {
    
    var body: some View {
        CustomMap()
            .edgesIgnoringSafeArea(.all)
        
        
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}


struct CustomMap: UIViewRepresentable {
    
    @State var mapView = MKMapView()
    @State var manager = CLLocationManager()
    
    func makeCoordinator() -> Coordinator {
        CustomMap.Coordinator(mapView: $mapView, manager: $manager)
    }
    
    
    func makeUIView(context: Context) -> MKMapView {
        if manager.authorizationStatus == .notDetermined || manager.authorizationStatus == .denied {
            manager.requestWhenInUseAuthorization()
            manager.startUpdatingLocation()
        }
        mapView.delegate = context.coordinator
        manager.delegate = context.coordinator
        mapView.showsUserLocation = true
        mapView.isZoomEnabled = true
        
        
        return mapView
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
    }
    
    class Coordinator: NSObject, MKMapViewDelegate, CLLocationManagerDelegate {
        
        @Binding var mapView : MKMapView
        @Binding var manager : CLLocationManager
        @ObservedObject var carObject = CarViewModel()
        
        init(mapView: Binding<MKMapView>, manager: Binding<CLLocationManager>) {
            self._mapView = mapView
            self._manager = manager
        }
        
        
        
        func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
            mapView.removeAnnotations(mapView.annotations)
            for i in 0..<carObject.carsArray.count{
                let car = CLLocationCoordinate2D(latitude: CLLocationDegrees(carObject.carsArray[i].lan), longitude: CLLocationDegrees(carObject.carsArray[i].lon))
                let add = MKPointAnnotation()
                
                add.coordinate = car
                mapView.addAnnotation(add)
            }
        }
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            if let coordinate = locations.first?.coordinate {
                let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 100000, longitudinalMeters: 100000)
                
                mapView.setRegion(region, animated: true)
                manager.stopUpdatingLocation()
                
            }
        }
        
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            if annotation is MKUserLocation{
                let add = MKAnnotationView()
                add.image = UIImage(systemName: "house")
                return add
            }
            let add = MKAnnotationView()
            add.image = UIImage(systemName: "lock")
            return add
        }
    }
}
